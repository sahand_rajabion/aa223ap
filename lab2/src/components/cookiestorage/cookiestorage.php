<?php
	namespace cookie;

	class CookieStorage {
		// vars for storage of login info
		private static $cookieIdentifier = "dv408";

		// saves username as text for auto complete of forms
		public function save ($string) {
			setcookie(self::$cookieIdentifier.'Username', $string, time()+(60*60*24*30));
			$_COOKIE[self::$cookieIdentifier.'Username'] =  $string;
		}
		
		// remembers user credentials, for auto login
		public function remember ($username, $password) {
			$datestring = time()+(60*60*24*30);
			file_put_contents('datestring.txt', $datestring);
			$useragent = $_SERVER["HTTP_USER_AGENT"];
			file_put_contents('useragent.txt', $useragent);
			setcookie(self::$cookieIdentifier.'rememberedCredentialsName', $username, time()+(60*60*24*30));
			$_COOKIE[self::$cookieIdentifier.'rememberedCredentialsName'] =  $username;
			setcookie(self::$cookieIdentifier.'rememberedCredentialsPw', $password, time()+(60*60*24*30));
			$_COOKIE[self::$cookieIdentifier.'rememberedCredentialsPw'] =  $password;
		}

		/* 
		* @description used for checking existing saved non-crypt username for auto completion
		* @return returns an associative position in $_COOKIE 
		*/
		public function getUserCookie() {
			if (!isset($_COOKIE[self::$cookieIdentifier.'Username'])) {
				return;
			}
			else {
				return self::$cookieIdentifier.'Username';
			}
		}

		/* 
		* @description used for logging in automatically, returns the password
		* @return returns an associative position of crypted md5-hash password found in $_COOKIE 
		*/
		public function getRememberedPassword () {
			if (!isset($_COOKIE[self::$cookieIdentifier.'rememberedCredentialsPw'])) {
				return;
			}
			else {
				return self::$cookieIdentifier.'rememberedCredentialsPw';
			}
		}

		/* 
		* @description used for logging in automatically, returns the username
		* @return returns an associative position of username found in $_COOKIE 
		*/
		public function getRememberedUsername () {
			if (!isset($_COOKIE[self::$cookieIdentifier.'rememberedCredentialsName'])) {
				return;
			}
			else {
				return self::$cookieIdentifier.'rememberedCredentialsName';
			}
		}

		/* 
		* @description used for logging in automatically, checks if saved cookies exists
		* @return true if found, false if none are saved
		*/
		public function rememberedCredentialsExists () {
			if (isset($_COOKIE[self::$cookieIdentifier.'rememberedCredentialsName']) || isset($_COOKIE[self::$cookieIdentifier.'rememberedCredentialsPw'])) {
				return true;
			}
			else {
				return false;
			}
		}

		/* 
		* @description used for when logging out, removes the cookies if found
		* @return returns if none are found
		*/
		public function clearCredentials () {
			if (isset($_COOKIE[self::$cookieIdentifier.'rememberedCredentialsName']) && isset($_COOKIE[self::$cookieIdentifier.'rememberedCredentialsPw'])) {
				$_COOKIE[self::$cookieIdentifier.'rememberedCredentialsName'] = "";
				setcookie(self::$cookieIdentifier.'rememberedCredentialsName', "", time()-1);
				$_COOKIE[self::$cookieIdentifier.'rememberedCredentialsPw'] = "";
				setcookie(self::$cookieIdentifier.'rememberedCredentialsPw', "", time()-1);
			}
			else {
				return;
			}
		}
		/* 
		* @description used for preventing cookie time manipulation
		* @return returns a saved string of cookie expiration
		*/
		public function getSavedDate () {
			return file_get_contents('datestring.txt');
		}

		public function getSavedUserAgent() {
			return file_get_contents('useragent.txt');
		}
	}