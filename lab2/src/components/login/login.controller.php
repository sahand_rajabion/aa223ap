<?php
	namespace login;
	
	require_once("login.model.php");
	require_once("login.view.php");

	class LoginController {
		private $model;
		private $view;
				
		public function __construct () {
			$this->model = new \login\LoginModel();
			$this->view = new \login\LoginView($this->model);
		}

		/*
		* @description login renders one of two views
		* $view->loggedIn() is rendered if user authenticates
		* $view->loggedOut() for all other input errors
		*/
		public function login () {
			
			// init login
			if (isset($_POST["LoginView::Submit"])) {

					// set login credentials, stored or new depending on previous input
					$this->view->setLoginCredentials();
					
					// remember user credentials if the user checks box
					if (isset($_POST["LoginView::Checked"]))	{
						$this->view->cookie->remember($this->view->getUsername(), $this->view->getPassword());
					}

					// render backend view if logged in
					if ($this->model->authenticate($this->view->getPassword(), $this->view->getUsername())) {
							$this->userIsLoggedIn();
					}

					// render form() once again if authentication fails
					else {
							$this->userIsLoggedOut();
					}
				}

				else {
					// if user is already logged in and refreshes, 
					// render backend view
					if (isset($_SESSION["loggedIn"]) && $_SESSION["loggedIn"] == true) {
						$this->userIsLoggedIn();
					}
					// otherwise user is logged out,
					// render login form()
					else {
						$this->userIsLoggedOut();
				}
			}
		}

		// makes sure visitors user agent matches the stored user agent
		// if true, user isn't logged in
		private function userAgentsDoesntMatch () {
			return $this->view->cookie->getSavedUserAgent() != $_SERVER["HTTP_USER_AGENT"];
		}

		// checks for expired cookie 
		private function userCookieHasExpired () {
			return time() > $this->view->cookie->getSavedDate();
		}

		public function autologin () {
				if ($this->userAgentsDoesntMatch() || $this->userCookieHasExpired()) {
					$this->view->removeStoredCookies();
					$this->view->loggedOut(
							$this->view->title,
							$this->view->form(),
							"Felaktig information i cookie",
							$this->view->time
					);
					return;
				}

				// check for stored cookies for autologin,
				// if found, login using those credentials 
				
				$_SESSION["autologin"] = true;
				
				// sets username and password to stored credentials
				$this->view->loadStoredCookies();
				
				// tries to log in with stored credentials
				if ($this->model->authenticate($this->view->getPassword(), $this->view->getUsername())) {
					$this->userIsLoggedIn();
				}
				
				// auth failed with stored credentials, user is logged out
				else {
					$this->view->removeStoredCookies();
					$this->view->loggedOut(
							$this->view->title,
							$this->view->form(),
							"Felaktig information i cookie",
							$this->view->time
					);
				}
		}
		
		/*
		* @description abstraction for rendering $view->loggedOut
		*/
		public function userIsLoggedOut () {
			if (isset($_SESSION["logOutMessage"])) {
				$feedback = $_SESSION["logOutMessage"];
			}
			else {
				$feedback = $this->model->getCurrentFeedback();
			}

			$this->view->loggedOut(
						$this->view->title,
						$this->view->form(),
						$feedback,
						$this->view->time
			);
		}

		/*
		* @description abstraction for rendering $view->loggedIn
		*/
		public function userIsLoggedIn () {
			$this->view->loggedIn(
				$this->model->currentFeedback						
			);
		}

		// destroys session
		public function destroySession() {
			$this->model->destroy();
		}

		// logs out user by changing SESSION states,
		// removes all cookies stored for login by users (username is still saved for automatic form input),
		// sets current feedback message to blank,
		// renders logged out view
		public function logOutUser () {
			$this->model->logOut();
			$this->view->removeStoredCookies();
			$this->model->currentFeedback = "";
			$this->userIsLoggedOut();
		}

		public function setHeaderToIndex () {
			$this->view->redirect('./index.php');
		}

		// checks for existing cookies with stored credentials,
		// if found, logs in using them
		public function userShouldLoginAutomatically () {
			return $this->view->checkForExistingCredentials();
		}
}