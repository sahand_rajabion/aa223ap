<?php 
	namespace login;
	
	class LoginModel {

		// this array stores all of the messages served to the client
		// all new messages should go here
		private $feedback = array(
			"userMissing" => "Användarnamn saknas",
			"passwordMissing" => "Lösenord saknas",
			"userInputIsWrong" => "Felaktigt användarnamn och/eller lösenord",
			"userLoggedIn" => "Inloggning lyckades",
			"alreadyLoggedIn" => "Admin är inloggad",
			"rememberUser" => "Inloggning lyckades och vi kommer ihåg dig nästa gång",
			"cookieLoginSuccessful" => "Inloggning lyckades via cookies",
			"corruptCookie" => "Felaktig information i cookie"
		);

		// vars used for authentication
		// you probably shouldn't store your passwords as text...
		private $password = "Password";
		private $username = "Admin";
		private $salt = "";
		public $currentFeedback = "";
		
		/*
		* @param String $password
		* @param String $username
		* @description sets feedback to correct message, returns true if user auths
		 */
		public function authenticate ($password, $username) {
			// authentication of login
			if ($password == crypt($this->password, $this->salt) && $username == $this->username) {
				// user hasn't already logged in
				if (!isset($_SESSION["loggedIn"]) && !isset($_SESSION["autologin"])) {
					$this->currentFeedback = $this->feedback["userLoggedIn"];
				
				// user wants to save credentials for autologin
				if (isset($_POST["LoginView::Checked"]))	{
					$this->currentFeedback = $this->feedback["rememberUser"];
					$_SESSION["autologin"] = true;
				}			
			}
			
			// user is already logged in, 
			// if logged in with auto login one feedback,
			// otherwise nothing
			else {
				if (isset($_SESSION["autologin"]) && $_SESSION["autologin"] = true) {
					$this->currentFeedback = $this->feedback["cookieLoginSuccessful"];
				}
				else {
					$this->currentFeedback = "";
				}
			}
			
			// user logs in successfully
			$_SESSION["loggedIn"] = true;
			return true;
			}

			// username is missing,
			// or both fields are empty
			if (!isset($username) || $username == "") {
				$this->currentFeedback = $this->feedback["userMissing"];
				return false;
			}

			// password's missing
			if (!isset($password) || $password == "") {
				$this->currentFeedback = $this->feedback["passwordMissing"];
				return false;
			}

			// incorrect credentials
			else {
				$this->currentFeedback = $this->feedback["userInputIsWrong"];
				return false;
			}	 
		}

		// returns a salt set from http user agent
		public function getSalt() {
			return $this->salt;
		}

		/*
		*	@description contains feedback message corresponding to the user's input 
		*/
		public function getCurrentFeedback() {
			return $this->currentFeedback;
		}

		// sets state of logged in to false 
		// sets logout message to display when logged out
		public function logOut() {
			$_SESSION["loggedIn"] = false;
			$_SESSION["logOutMessage"] = "Du har nu loggat ut";
		}

		// abstraction used from controller to kill session
		public function destroy() {
			session_destroy();
		}
		
		// starts session
		// sets salt to http user agent to prevent session stealing
		public function __construct () {
			session_start();
			$this->salt = $_SERVER["HTTP_USER_AGENT"];
		}
	}