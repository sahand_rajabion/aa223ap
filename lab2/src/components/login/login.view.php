<?php 
	namespace login;
		require_once(dirname(__FILE__)."/../cookiestorage/cookiestorage.php");
	class LoginView {
		// $model stores model used in controller
		private $model;
		private $userinput = array (
			"username" => "",
			"password" => ""
			);
		public $cookie;
		private $userCookie;

		// $time, $title, $form are used as templates when rendering page
		public $time;
		public $title = "Ej inloggad";
		
		// renders form for echoing when rendered. 
		// sets username's value to previously stored if it exists
		public function form () {
			if (isset($_COOKIE[$this->cookie->getUserCookie()])) {
				$username = $_COOKIE[$this->cookie->getUserCookie()];
			}
			else { 
				$username = "";
			}
			return "<form method='post' enctype='multipart/form-data'>
			<fieldset>
			<legend>Login - Skriv in användarnamn och lösenord</legend>
			<label for='userNameID'>Användarnamn :</label>
			<input type='text' size='20' name='LoginView::User' id='userNameID' value='$username'>
			<label for='passwordID'>Lösenord  :</label>
			<input type='password' size='20' name='LoginView::Password' id='passwordID' value=''>
			<label for='AutologinID'>Håll mig inloggad:</label>
			<input type='checkbox' name='LoginView::Checked' id='AutologinID'>
			<input type='submit' name='LoginView::Submit' value='Logga in'>
			</fieldset>
			</form>";
		}

		/*
		* @description sets username and password based on userinput,
		*              maps to the variables saved in $_POST 
		*/
		public function setLoginCredentials () {
			$this->userinput["username"] = $_POST["LoginView::User"];
			$this->userinput["password"] = crypt($_POST["LoginView::Password"], $this->model->getSalt());
			return;
		}

		public function loadStoredCookies () {
			$this->userinput["username"] = $_COOKIE[$this->cookie->getRememberedUsername()];
			$this->userinput["password"] = $_COOKIE[$this->cookie->getRememberedPassword()];
		}

		/*
		* @param String $title
		* @param String $body
		* @param String $feedbackMessage
		* @param String $footer
		* @description renders page, sets title, body and footer dynamically.
		*/
		public function render ($title, $body, $feedbackMessage, $footer) {
				echo "<!doctype html>
						<html lang='sv'>
						<head>
						<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
						<title> Laboration 2 av aa223ap </title>
						<link href='https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css' rel='stylesheet' type='text/css'> 
						</head>
						<body>
						<div style='display: block; margin: 50px;'>
						<h1>Laboration 2 av aa223ap</h1>
						<h2> $title </h2>
						<p><a href='register'>Registrera ny användare</a></p><br>

						$body

						<span><strong>$feedbackMessage</strong></span> 
						<footer><p> $footer </p></footer></div></body></html>";		
		}

		/*
		*	@param String $feedback
		*	@param String $username
		*	@description function renders view for user that authenticates
		*/
		public function loggedIn ($feedback) {
			// saves cookie if it doesn't already exists
			// would overwrite cookie as blank otherwise
			if (!isset($_COOKIE[$this->cookie->getUserCookie()])) {
				$this->cookie->save($this->getUsername());
				$username = $_COOKIE[$this->cookie->getUserCookie()];
			}
			else { 
				$username = $_COOKIE[$this->cookie->getUserCookie()];
			}
			$title = "$username är inloggad";
			$logOutLink = "<p><a href='?logout'>Logga ut</a></p>";
			$this->render($title, $logOutLink, $feedback, $this->time);
		}

		/*
		* @param String $title
		* @param String $body
		* @param String $feedbackMessage
		* @param String $footer
		*	@description function renders view for user that fails to authenticate
		*/
		public function loggedOut ($title, $form, $feedback, $footer) {
			if (!isset($_COOKIE[$this->cookie->getUserCookie()])) {
				$this->cookie->save($this->getUsername());
			}
			$this->render($title, $this->form(), $feedback, $footer);
		}

		/*
		* @param LoginModel $loginmodel maps to same model that's used in LoginController
		* @description $this->time gets set to current time when view is constructed
		* @description $this->cookies stores auth details
		*/
		public function __construct (LoginModel $loginModel) {
			$this->model = $loginModel;
			$this->time = $this->setTime();
			$this->cookie = new \cookie\CookieStorage();
			// sets username to display if cookie exists
			// is null otherwise
			if (isset($_COOKIE[$this->cookie->getUserCookie()])) {
				$this->userCookie = $_COOKIE[$this->cookie->getUserCookie()];
			}
			else {
				return;
			}
		}

		/*
		 * @return username retrieved from $_POST
		 */
		public function getUsername () {
			return $this->userinput["username"];
		}

		/*
		 * @return password retrieved from $_POST
		 */
		public function getPassword () {
			return $this->userinput["password"];
		}

		/* 
		* @description used for logging in automatically, checks for saved cookies
		* @return returns true if not logged in and cookies with credentials exist
		*/
		public function checkForExistingCredentials () {
			return $this->cookie->rememberedCredentialsExists() && !isset($_SESSION["loggedIn"]);
		}

		/* 
		* @description used when logging out
		* @return kills all saved cookies
		*/
		public function removeStoredCookies () {
			$this->cookie->clearCredentials();
		}
		
		/*
		* @description changes header location
		*/
		public function redirect ($location) {
        header('Location: ' . $location);
    }

		/*
		* @return a locale timestring mapped to swedish
		*/
		public function setTime() {
			date_default_timezone_set('Europe/Stockholm');
			setlocale(LC_ALL,"sv_SE.UTF8");
			return strftime("%A, den %d %B år %Y. Klockan är [%X].");
		}
	}