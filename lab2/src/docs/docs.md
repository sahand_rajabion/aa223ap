# Documentation for lab2

> If anything is unclear, contact aa223ap

## Naming conventions

This implementation of lab2 tries to employ strict naming of code and files.

## Filenames

All files should be named by their usage, followed by their responsibilities, for example:

```
/app
|_  portfolio.view.php
|_ 	portfolio.controller.php
|_	portfolio.model.php
|_	portfolio.test.php
```

## Classnames

Classnames should be using [CamelCase](http://en.wikipedia.org/wiki/CamelCase).

Example: 

```
class PortfolioView
class PortfolioController
class PortfolioModel
```

## Folder structure

Main files< goes into `/app`. Other files should follow the MVC-pattern and be written as requirable (is that a word?) components and placed in `/components/componentname`.

## Return values

Return values should be named.

__Right__: `return $allMyFruit;`

__Wrong__: `return $ret;`




