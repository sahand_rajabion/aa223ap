<?php 
	require_once("./components/login/login.controller.php");
	
	$loginController = new \login\LoginController();
	
	// check for existing credentials
	if ($loginController->userShouldLoginAutomatically()) {
		$loginController->autologin();
	}
	else {
		// check for logout 		
		if (isset($_GET["logout"]) && isset($_SESSION["loggedIn"]) && $_SESSION["loggedIn"] == true) {
			$loginController->logOutUser();
			$loginController->destroySession();
		}
		// login user otherwise
	  else {
	   	$loginController->login();
	   	if (isset($_GET["logout"]) && isset($_SESSION["loggedIn"]) && $_SESSION["loggedIn"] == true) {
	   		$loginController->setHeaderToIndex();
	   	}
		}
	}
	
