<?php 
	
	namespace profilerobot;

	require_once("profilerobot.model.php");
	require_once("profilerobot.view.php");

	class ProfileRobotController {
		/* shared model with view */
		private $model;
		private $view;

		/* initiates the mvc */
		public function __construct () {
			$this->model = new \profilerobot\ProfileRobotModel();
			$this->view = new \profilerobot\ProfileRobotView($this->model);
		}

		/* @description page is rendered by echoing from view */
		public function renderPage () {
			// render takes title, body, button and footer			
			$this->view->render(
			// h1	
			$this->view->renderH1("<img src='img/robot.svg' alt='Profile Robot - so darn helpful'> Profile Robot"),
			// renders two paragraphs as the body
			($this->view->renderParagraph("Choose your nicest looking self and I'll do all of the work.").
			 $this->view->renderParagraph(" Start by choosing your image file.")),
			// fully styled form with bootstrap, change classes if styling should change
			$this->view->renderFileUpload("Upload", "Only images in .png and .jpg format are allowed. Filesize should be less than 500kb.", "btn btn-block btn-info btn-lg", false),
			// echoes in footer
			$this->view->renderFooter("Idea and web development by <a href='https://github.com/antonkandersson/' title='Anton K. Andersson on Github'>Anton K. Andersson</a>. Robot designed by <a href='http://www.thenounproject.com/iammateus'>Mateus Leal</a> from the <a href='http://www.thenounproject.com'>Noun Project</a>.")
			);
		}

	}