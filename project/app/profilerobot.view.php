<?php 
	namespace profilerobot;

	require_once('components/html/html.view.php');
	
	class ProfileRobotView extends \html\HTMLView {
		
		/* shared model with controller */
		private $model;

		public function __construct (ProfileRobotModel $model) {
			$this->model = $model;
		}

		public function render ($title, $body, $button, $footer) {
			echo "<!doctype html>
			<html lang='sv'>
			<head>
			<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
			<meta name='viewport' content='width=device-width, initial-scale=1'>
			<title>$title</title>
			<link href='https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css' rel='stylesheet' type='text/css'> 
			</head>
			<body class='container-fluid' style='max-width: 40%; margin: 0 auto;'>
			$title
			<blockquote>$body</blockquote>
			$button 
			$footer";
		}
	}