<?php

	namespace html;
	require_once("./components/errorhandling/errorhandler.php");

	class HTMLView {
		private $error;
		public function __construct () {
			$this->error = new \error\ErrorHandler();

		}

		/* 
		* @param String $text to use on button
		* @param String $class optional parameter to use as class attribute for HTML
		*/
		public function renderButton ($text, $class = null) {
			if ($class == null) {
				return "<button> $text </button>";
			}
			else {
				return "<button class='$class'> $text </button>";
			}
		}
		
		/* 
		* @param String $text to use as H1
		* @param String $class optional parameter to use as class attribute for HTML
		*/
		public function renderH1 ($text, $class = null) {
			if ($class == null) {
				return "<h1> $text </h1>";
			}
			else {
				return "<h1 class='$class'> $text </h1>";
			}
		}

		/* 
		* @param String $text to use as H2
		* @param String $class optional parameter to use as class attribute for HTML
		*/
		public function renderH2 ($text, $class = null) {
			if ($class == null) {
				return "<h2> $text </h2>";
			}
			else {
				return "<h2 class='$class'> $text </h2>";
			}
		}

		/* 
		* @param String $text to use as H3
		* @param String $class optional parameter to use as class attribute for HTML
		*/
		public function renderH3 ($text, $class = null) {
			if ($class == null) {
				return "<h3> $text </h3>";
			}
			else {
				return "<h3 class='$class'> $text </h3>";
			}
		}

		/* 
		* @param String $text to use as copy for paragraph
		* @param String $class optional parameter to use as class attribute for HTML
		*/
		public function renderParagraph ($text, $class = null) {
			if ($class == null) {
				return "<p> $text </p>";
			}
			else {
				return "<p class='$class'> $text </p>";
			}
		}

		/* 
		* @param String $text to use as text for input button
		* @param String $helptext optional parameter to use as explanatory helptext for user
		* @param String $class optional parameter to use as class attribute for label
		*/
		public function renderFileUpload ($text, $helptext = null, $class = null) {
			$label = $text;
			$identifier = $text.="FileForm";
			
			// render neither class or helptext if not passed in
			if ($helptext == null && $class == null) {
				return 
				"<div class='form-group'>
	    	<label for='$identifier'>$label</label>
	    	<input type='file' id='$identifier'>
	    	</div>";
			}

			// render both class and helptext if passed in
			if ($helptext && $class) {
				return 
				"<div class='form-group'>
	    	<label for='$identifier'>$label</label>
	    	<input type='file' id='$identifier' class='$class'>
	    	<p class='help-block'>$helptext</p>
	  		</div>";
			}

			// render helptext but skip class if class isn't passed in
			if ($helptext && !$class) {
				return 
				"<div class='form-group'>
	    	<label for='$identifier'>$label</label>
	    	<input type='file' id='$identifier'>
	    	<p class='help-block'>$helptext</p>
	  		</div>";
			}

			else {
				return 
				"<div class='form-group'>
	    	<label for='$identifier'>$label</label>
	    	<input type='file' id='$identifier' class='$class'>
	    	</div>";
			}
		}		

		/* 
		* @param String $text to use as copy for paragraph
		* @param String $class optional parameter to use as class attribute for HTML
		*/
		public function renderFooter ($text, $class = null) {
			if ($class == null) {
				return "<footer> $text </footer>";
			}
			else {
				return "<footer class='$class'> $text </footer>";
			}
		}
	}