#Profile Robot

> Profile Robot makes creating profile pictures for all of your social media easy.

The app takes one original image, processes it and then generates profile pictures for the three biggest social media sites:

* [Facebook](https://facebook.com)

* [Google+](https://plus.google.com)

* [Twitter](https://twitter.com)

