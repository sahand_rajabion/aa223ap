# All my profiles
Use cases for _All my profiles_

## <a id="uc1">UC 1</a>

> The user has just arrived at page

* __UC 1.1__  
User visits page. Page displays a form containing a file upload textfield and a button named "Upload". Page also displays a short info text about the usage of the web app.

* __UC 1.2__  
User clicks on file upload. A file navigation popup of the users local machine is displayed.

* __UC 1.3__  
User can navigate her own file system. 

* __UC 1.4__  
User can choose an image file from her own file system.

* __UC 1.5__  
User can only choose an image file and only images in JPG or PNG format.

* __UC 1.6__  
User chooses a correct image file. The file's system reference is displayed in the file upload form.

## <a id="uc2">UC 2</a>

> The user has chosen a correct image. 

* __UC 2.1__  
The user has chosen a correct image. The system reference is displayed. The user can click on the upload button.

* __UC 2.2__ 
The user has clicked on the upload button. The system saves the image to the server in four profile picture formats. The profile pictures are generated as Facebook profile, Google+ profile, Twitter profile and the original image. 

* __UC 2.3__  
The system has saved the images as the original image's filename plus a companyname, ie `original_twitter.png`. The saved images are displayed to the user. Each image has a button named Download and it's correct company logo.

## <a id="uc3">UC 3</a>

> The user has clicked on upload, the page has displayed the different profile pictures.

* __UC 3.1__  
The user right clicks on a profile picture. The user can save the picture by using the native "Save image as...".

* __UC 3.2__  
The user clicks the download button. A download starts containing the correct image.